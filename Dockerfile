FROM debian:stable
LABEL maintainer="6543"

RUN apt update && apt dist-upgrade -y
RUN apt install -y bash git wget openssh-client gnupg2

RUN wget -qO - https://artifacts.crowdin.com/repo/GPG-KEY-crowdin | apt-key add -
RUN echo "deb https://artifacts.crowdin.com/repo/deb/ /" > /etc/apt/sources.list.d/crowdin.list
RUN apt update && apt install -y crowdin
